package main

import "C"

import (
  "interview.stably.io/pkg/helper"
  "interview.stably.io/pkg/api"
  "net/http"
  "testing"
  "fmt"
)

var server *api.Multiplexer = nil

//export PrintFromGo
func PrintFromGo(message *C.char) *C.char {
  return C.CString(fmt.Sprintf(C.GoString(message)))
}

//export StartHttpServer
func StartHttpServer(port int) bool {
  if server != nil && server.IsStarted() {
    server.Stop()
  }

  if port > 0 {
    server = helper.NewBuildMultiplexer().
      SetPort(int(port)).
      AddNewHandler("dummy", func(req *http.Request) (resp string, err error) {
        return "", nil
      }).
      Build()
  } else {
    return false
  }

  if err := server.StartNoWaiting(); err != nil {
    return false
  } else {
    return true
  }
}

//export StopHttpServer
func StopHttpServer() {
  if server.IsStarted() {
    server.Stop()
  }
}

//export MakeGetRequest
func MakeGetRequest(url *C.char) bool {
  if _, err := http.Get(C.GoString(url)); err != nil {
    return false
  }

  return true
}

func TestStartStopServer(t *testing.T) {
  StartHttpServer(8080)

  if _, err := http.Get("http://127.0.0.1:8080/dummy"); err != nil {
    t.Errorf("http.Get to dummy got error %s", err.Error())
  }

  StopHttpServer()
}

func BenchmarkStartStopServer(b *testing.B) {
  StartHttpServer(8080)

  b.RunParallel(func(pb *testing.PB) {
    for pb.Next() {
      if _, err := http.Get("http://127.0.0.1:8080/dummy"); err != nil {
        fmt.Printf("got error %v", err)
      }
    }
  })

  StopHttpServer()
}

func main() {}
