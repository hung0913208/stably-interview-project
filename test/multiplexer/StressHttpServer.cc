#include <assert.h>

#include "benchmark/benchmark.h"
#include "test/multiplexer/go_http_server.h"

static void FloodRequest(benchmark::State &state) {
  for (auto _ : state) {
    assert(MakeGetRequest("http://127.0.0.1:8080/dummy"));
  }
}

// Register the function as a benchmark
BENCHMARK(FloodRequest);

int main(int argc, char **argv) {
  assert(StartHttpServer(8080));

  ::benchmark::Initialize(&argc, argv);
  ::benchmark::RunSpecifiedBenchmarks();

  StopHttpServer();
  return 0;
}
