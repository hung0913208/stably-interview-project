#include "test/multiplexer/go_http_server.h"
#include "gtest/gtest.h"

#include <arpa/inet.h>
#include <chrono>
#include <netdb.h>
#include <netinet/in.h>
#include <string>
#include <sys/socket.h>
#include <sys/types.h>
#include <thread>
#include <unistd.h>

using namespace std;

bool IsPortOpened(int port) {
  struct sockaddr_in servaddr {};
  bool ret{false};
  int s{socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)};

  if (s == -1)
    return false;

  memset(&servaddr, 0, sizeof(servaddr));
  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(port);
  servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");

  if (connect(s, (struct sockaddr *)&servaddr, sizeof(servaddr)) == -1) {
    cout << strerror(errno);
    ret = false;
  } else
    ret = true;

  close(s);
  return ret;
}

TEST(Brigde, CallGoFromCpp) {
#define MESSAGE "hello go, i'm from gtest of c++"

  EXPECT_EQ(string{PrintFromGo(MESSAGE)}, MESSAGE);
}

TEST(Http, StartStopServer) {
  EXPECT_TRUE(StartHttpServer(8080));

  EXPECT_TRUE(IsPortOpened(8080));
  EXPECT_TRUE(MakeGetRequest("http://127.0.0.1:8080/dummy"));
  StopHttpServer();
}
