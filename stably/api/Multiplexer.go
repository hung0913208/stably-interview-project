package api

import (
  "sync/atomic"
  "net/http"
  "os/signal"
  "context"
  "errors"
  "time"
  "log"
  "fmt"
  "os"
)

const Stopped = 0
const Stopping = 1
const Started = 2
const Halted = 3

type HandlerCallback func(request *http.Request) (response string, err error)

type Multiplexer struct {
  /* @NOTE: specify basic information to start a new multiplexer */
  handlers map[string]HandlerCallback
  port int

  /* @NOTE: flag which indicate the self's status*/
  running int32

  /* @NOTE: these chan would be used to control the server by puting values
   * onto them to trigger specific events like closing the server or collecting
   * issue during handling traffics */
  issue chan error
  done chan bool
}

func (self *Multiplexer) GetHandlersPtr() map[string]HandlerCallback {
  if self.handlers == nil {
    self.handlers = make(map[string]HandlerCallback)
  }

  return self.handlers
}

func (self *Multiplexer) SetPort(port int) {
  self.port = port
}

func (self *Multiplexer) GetPort() int {
  return self.port
}

func (self *Multiplexer) IsStarted() bool {
  return atomic.CompareAndSwapInt32(&self.running, Started, self.running)
}

func (self *Multiplexer) IsStopping() bool {
  return atomic.CompareAndSwapInt32(&self.running, Stopping, self.running)
}

func (self *Multiplexer) IsStopped() bool {
  return atomic.CompareAndSwapInt32(&self.running, Stopped, self.running)
}

func (self *Multiplexer) IsHalted() bool {
  return atomic.CompareAndSwapInt32(&self.running, Halted, self.running)
}

func (self *Multiplexer) Start() error {
  return self.StartWithParameters(true, 5, 10, 15)
}

func (self *Multiplexer) StartNoWaiting() error {
  return self.StartWithParameters(false, 5, 10, 15)
}

func (self *Multiplexer) StartWithTimeout(
        recvTimeout, sendTimeout, idleTimeout int) error {
  return self.StartWithParameters(true, recvTimeout, sendTimeout,
                                  idleTimeout)
}

func (self *Multiplexer) StartWithParameters(needsWait bool,
        recvTimeout, sendTimeout, idleTimeout int) error {
  if len(self.handlers) == 0 {
    return errors.New("start without configuring any handlers")
  } else if self.port == 0 {
    /* @NOTE: by default, a new HTTP server will start at port 80 if we don't
     * specify any specific value */

    self.port = 80
  }

  router := http.NewServeMux()
  logger := log.New(os.Stdout, "http: ", log.LstdFlags)
  page_index := fmt.Sprint("http://127.0.0.1:", self.port)

  logger.Println("Server is starting...")

  for path, handler := range(self.handlers) {
    router.Handle(path, http.HandlerFunc(func(writer http.ResponseWriter,
                                              reader *http.Request) {
      /* @TODO: custom code to handle the cases when the handler is too slow to
       * be accepted or return error, logs and traceback to help for debuging */

      if reader.RequestURI == path {
        if response, err := handler(reader); err != nil {
          fmt.Fprintf(writer, "issue")
        } else {
          fmt.Fprintf(writer, response)
        }
      }
    }))
  }

  /* @NOTE: build an http server with timeouts so we could avoid flooding
   * requests */
  server := &http.Server{
    Addr: fmt.Sprintf(":%d", self.port),
    Handler: router,
    ErrorLog: logger,
    ReadTimeout:  time.Duration(recvTimeout) * time.Second,
    WriteTimeout: time.Duration(sendTimeout) * time.Second,
	IdleTimeout:  time.Duration(idleTimeout) * time.Second,
  }

  self.done = make(chan bool)
  self.issue = make(chan error, 1)

  go func() {
    atomic.StoreInt32(&self.running, Started)
    self.issue <- server.ListenAndServe()
  }()

  go func() {
    <-self.done
      /* @NOTE: to make sure we don't stuck forever at shutdowing state, we must
       * set the a deadline for it and the timeout could be double time of the
       * current idle timeout */

	  atomic.StoreInt32(&self.running, Stopping)
      ctx, cancel := context.WithTimeout(context.Background(),
                            2 * time.Duration(idleTimeout) * time.Second)
      defer cancel()

      server.SetKeepAlivesEnabled(false)

      if err := server.Shutdown(ctx); err != nil {
	    atomic.StoreInt32(&self.running, Halted)
      } else {
        atomic.StoreInt32(&self.running, Stopped)
      }
  }()

  var err error = nil

  if needsWait {
    /* @NOTE: most of the time, we will let the server to wait requests from
     * clients, it also handles the signal sigterm which is produced by using
     * CTRL-C to break the server from handling states, closing incoming
     * connection gently and avoid leaking client's file description */

    quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)

    select {
    case err := <-self.issue:
      return err
    case <-quit:
      close(self.done)
    }
  } else {
    /* @NOTE: if we don't want to lock the main-thread at listening on HTTP port
     * so we could set flag needsWait to false. At this case, we must check for
     * sure that the port is started or not before*/

    for i := 0; i < 10; i++ {
      if _, err := http.Get(page_index); err != nil {
        time.Sleep(time.Millisecond)
      } else {
        break
      }
    }
  }

  return err
}

func (self *Multiplexer) Stop() {
  close(self.done)
}
