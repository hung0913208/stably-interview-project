package helper

import (
  "interview.stably.io/pkg/api"
  "fmt"
)

type BuildMultiplexer struct {
  result api.Multiplexer
}


func (builder *BuildMultiplexer) SetPort(port int) *BuildMultiplexer {
  builder.result.SetPort(port)
  return builder
}

func (builder *BuildMultiplexer) AddNewHandler(path string,
      handler api.HandlerCallback) *BuildMultiplexer {
  if _, isFound := builder.result.GetHandlersPtr()[path]; isFound {
    panic(fmt.Sprintf("%s exists, can't add this new one", path))
  } else {
    builder.result.GetHandlersPtr()[path] = handler
  }

  return builder
}

func (builder *BuildMultiplexer) EditExisedHandler(path string,
      handler api.HandlerCallback) *BuildMultiplexer {
  if _, isFound := builder.result.GetHandlersPtr()[path]; isFound {
    delete(builder.result.GetHandlersPtr(), path)
  } else {
    panic(fmt.Sprintf("%s doesn't exist, please create it first", path))
  }

  return builder
}

func (builder *BuildMultiplexer) DelHandler(path string,
      handler api.HandlerCallback) *BuildMultiplexer {
  if _, isFound := builder.result.GetHandlersPtr()[path]; isFound {
    builder.result.GetHandlersPtr()[path] = handler
  } else {
    panic(fmt.Sprintf("%s doesn't exist, please create it first", path))
  }

  return builder
}

func (builder *BuildMultiplexer) Build() *api.Multiplexer {
  return &builder.result
}

func NewBuildMultiplexer() *BuildMultiplexer {
  return &BuildMultiplexer{}
}
